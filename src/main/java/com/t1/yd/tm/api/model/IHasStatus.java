package com.t1.yd.tm.api.model;

import com.t1.yd.tm.enumerated.Status;

public interface IHasStatus {

    void setStatus(Status status);

    Status getStatus();

}
